app_dir=$1
sudo apt-get update
sudo apt-get -y install nginx php5-fpm php5-cli php5-mcrypt git
sudo sed -i -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php5/fpm/php.ini
sudo php5enmod mcrypt
sudo service php5-fpm restart
sudo mkdir -p $app_dir
cd /etc/nginx/sites-available/
sudo wget -N https://bitbucket.org/mashpy/server-editor-installer/raw/24076e433991f0dc5e84f345618bcd6cfaa32419/laravel/config/default
sudo sed -i -e "s|servereditor_app_dir|$app_dir|" default
sudo service nginx restart
cd ~
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
sudo composer create-project laravel/laravel $app_dir
sudo chown -R :www-data $app_dir
sudo chmod -R 775 $app_dir/storage

